const fs = require("fs");

let fileModel = {};

let variants = {};

for(let i = 0; i < 50; i++) {
    let stone_variant = Math.floor(i / 10);
    variants["progress=" + i] = [
        { "model": "moltencore_mining:block/dense_stone_" + stone_variant },
        { "model": "moltencore_mining:block/dense_stone_" + stone_variant, "y": 180 },
        { "model": "moltencore_mining:block/dense_stone_" + stone_variant + "_mirrored" },
        { "model": "moltencore_mining:block/dense_stone_" + stone_variant + "_mirrored", "y": 180 },
    ];
}

fileModel["variants"] = variants;

let output = JSON.stringify(fileModel, null, 2);

console.log(output);

let path = "./moltencore-mining/src/main/resources/assets/moltencore_mining/blockstates/dense_stone.json";
fs.writeFile(
    path,
    output,
    function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("Output written to " + path);
    }
);
