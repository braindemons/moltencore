const util = require('util');
const exec = util.promisify(require('child_process').exec);

let substance = "C:\\Program Files\\Allegorithmic\\Substance Designer\\";

let stone_sbs = "./docs/dense_stone.sbs";
let stone_sbsar = "./docs/dense_stone.sbsar";
let output_dir = "./moltencore-mining/src/main/resources/assets/moltencore_mining/textures/block";

async function run_cooker() {
    const { stdout1, stderr1 } = await exec(
        "\"" + substance + "sbscooker.exe\" " +
        "--inputs " + stone_sbs + " " +
        "--includes \"" + substance + "resources\\packages\" " +
        "--output-path " + "./docs"
    );

    console.log("stdout:", stdout1);
    console.log("stderr:", stderr1);
}

async function run_render(output_name, cracks_size) {
    const { stdout2, stderr2 } = await exec(
        "\"" + substance + "sbsrender.exe\" render " +
        stone_sbsar + " " +
        "--output-format png " +
        "--output-path " + output_dir + " " +
        "--output-name " + output_name + " " +
        "--set-value $outputsize@5,5 " +
        "--set-value cracks_size@" + cracks_size
    );

    console.log("stdout:", stdout2);
    console.log("stderr:", stderr2);
}

async function build_textures() {
    console.log("Building sbs to sbsar");
    await run_cooker();

    console.log("Building sbsar to textures");
    await run_render("dense_stone_0", 0.0);
    await run_render("dense_stone_1", 0.5);
    await run_render("dense_stone_2", 1.25);
    await run_render("dense_stone_3", 2.0);
    await run_render("dense_stone_4", 3.0);
}

build_textures();