#!/bin/bash

function copy_artifacts {
  for file in $1/*
  do
    if [[ $file != *"-dev"* ]]; then
      echo "Copying $file..."
      cp $file ./
    fi
  done
}

copy_artifacts ./moltencore-mining/build/libs/
copy_artifacts ./moltencore-tools/build/libs/