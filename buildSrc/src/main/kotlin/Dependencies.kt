import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.kotlin.dsl.*

object Versions {
    // Check these on https://modmuss50.me/fabric.html
    const val minecraft = "1.14.4";
    const val yarn_mappings = "1.14.4+build.14";
    const val loader_version = "0.6.3+build.168";

    const val fabric_version = "0.4.1+build.245-1.14";
    const val fabric_kotlin_version = "1.3.50+build.3";
}

fun Project.fabricModProject() {
    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    repositories {
        maven {
            name = "Cotton"
            url = uri("http://server.bbkr.space:8081/artifactory/libs-release")
        }
    }

    dependencies {
        // Minecraft, mappings, and fabric loader
        "minecraft"("com.mojang:minecraft:${Versions.minecraft}")
        "mappings"("net.fabricmc:yarn:${Versions.yarn_mappings}")
        "modImplementation"("net.fabricmc:fabric-loader:${Versions.loader_version}")

        // Fabric API
        "modImplementation"("net.fabricmc.fabric-api:fabric-api:${Versions.fabric_version}")
        "modImplementation"("net.fabricmc:fabric-language-kotlin:${Versions.fabric_kotlin_version}")

        // Cotton APIs
        "modCompile"("io.github.cottonmc.cotton:cotton-logging:1.0.0-rc.1") {
            exclude(group = "net.fabricmc")
            exclude(group = "net.fabricmc.fabric-api")
        }
    }
}

fun Project.applyFabricModVersion(mod_version: String, sourceSets: SourceSetContainer) {
    version = mod_version

    tasks.named<Copy>("processResources") {
        inputs.property("version", mod_version)

        from(sourceSets["main"].resources.srcDirs) {
            include("fabric.mod.json")
            expand("version" to mod_version)
        }

        from(sourceSets["main"].resources.srcDirs) {
            exclude("fabric.mod.json")
        }
    }
}