import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// Minecraft, Fabric, and Loader versions are specified in buildSrc
// https://modmuss50.me/fabric.html

plugins {
    id("fabric-loom") version "0.2.6-SNAPSHOT"
    id("org.jetbrains.kotlin.jvm") version "1.3.50"
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

allprojects {
    tasks.withType<KotlinCompile>() {
        kotlinOptions.jvmTarget = "1.8"
    }
}

repositories {
    maven { url = uri("http://maven.fabricmc.net/") }
    maven { url = uri("https://minecraft.curseforge.com/api/maven") }
    maven { url = uri("http://tehnut.info/maven") }
}

// This is a dummy mod containing all the subproject mods
fabricModProject()

dependencies {
    if (!project.hasProperty("excludeutils")) {
        // Utility mods for debugging
        "modRuntime"("me.shedaniel:RoughlyEnoughItems:3.1.6+build.33") {
            exclude(group = "net.fabricmc")
            exclude(group = "net.fabricmc.fabric-api")
        }
        "modRuntime"("mcp.mobius.waila:Hwyla:1.14.2-1.9.17-66") {
            exclude(group = "net.fabricmc")
            exclude(group = "net.fabricmc.fabric-api")
        }
    }

    // Individual sub-mods
    compile(project(":moltencore-mining"))
    compile(project(":moltencore-tools"))
}

// ensure that the encoding is set to UTF-8, no matter what the system default is
// this fixes some edge cases with special characters not displaying correctly
// see http://yodaconditions.net/blog/fix-for-java-file-encoding-problems-with-gradle.html
tasks.withType<JavaCompile>() {
    options.encoding = "UTF-8"
}

// Loom will automatically attach sourcesJar to a RemapSourcesJar task and to the "build" task if it is present.
// If you remove this task, sources will not be generated.
tasks.register<Jar>("sourcesJar") {
    dependsOn("classes")

    archiveClassifier.set("sources")
    from(sourceSets["main"].allSource)
}

tasks.named<Jar>("jar") {
    from("LICENSE")
}
