# MoltenCore

MoltenCore is a multi-purpose kitchen sink mod, broken up into the following components:

- moltencore-mining - Mining gameplay rework.
- moltencore-tools - Tool gameplay rework.

You can use components individually without the others installed.


## Mining

**This component is under early development and not ready for use**

Changes mining gameplay to discourage excavation and strip-mining.
Encourages working with the terrain for construction, and strongly discourages large underground tunnel networks.

Instead, this component adds new ways to find and mine ores with higher long term rewards, making single mining
locations viable for a long time.


## Tools

**This component is under early development and not ready for use**

Removes punching trees and replaces the early-game wooden tools progression path.


## Installing

This mod depends on:

- Fabric Loader ([MultiMC](https://fabricmc.net/wiki/tutorial:install_with_multimc) or [fabricmc.net](https://fabricmc.net/use/))
- Fabric API ([CurseForge](https://www.curseforge.com/minecraft/mc-mods/fabric-api))
- Cotton ([CurseForge](https://www.curseforge.com/minecraft/mc-mods/cotton))


## Modpacks

You can use this mod in modpacks under the terms of the license provided.
Please add credit with a link to the GitHub page if you redistribute this mod.


## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
- MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
