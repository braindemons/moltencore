package net.braindemons.moltencore.mining

import net.fabricmc.fabric.api.event.registry.RegistryEntryAddedCallback
import net.minecraft.block.Blocks
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.minecraft.world.biome.Biome
import net.minecraft.world.gen.GenerationStep
import net.minecraft.world.gen.feature.ConfiguredFeature
import net.minecraft.world.gen.feature.DecoratedFeatureConfig
import net.minecraft.world.gen.feature.Feature
import net.minecraft.world.gen.feature.OreFeatureConfig
import net.minecraft.world.loot.context.LootContextTypes
import net.minecraft.world.loot.function.LootFunctions

@Suppress("unused")
fun init() {
    MoltenCoreMining.logger.info("Initializing")

    LootFunctions.register(DenseStoneLootFunction.Factory())

    LootContextTypeExt.addAllowed(LootContextTypes.GENERIC, MoltenCoreMiningLootContextParameters.explosion)
    LootContextTypeExt.addAllowed(LootContextTypes.BLOCK, MoltenCoreMiningLootContextParameters.explosion)

    Registry.register(
        Registry.BLOCK,
        Identifier(MoltenCoreMining.modId, "dense_stone"),
        MoltenCoreMiningBlocks.denseStone
    )

    Registry.register(
        Registry.ITEM,
        Identifier(MoltenCoreMining.modId, "dense_stone"),
        BlockItem(MoltenCoreMiningBlocks.denseStone, Item.Settings().group(ItemGroup.BUILDING_BLOCKS))
    )

    Registry.BIOME.forEach(::handleBiome)
    RegistryEntryAddedCallback.event(Registry.BIOME)
        .register(RegistryEntryAddedCallback { i, identifier, biome -> handleBiome(biome) })
}

private fun handleBiome(biome: Biome) {
    MoltenCoreMining.logger.info("Stripping replaced ore features from biome \"" + biome.translationKey + "\"")

    val features = biome.getFeaturesForStep(GenerationStep.Feature.UNDERGROUND_ORES)
    features.retainAll(::retainNotOres)
}

private fun retainNotOres(feature: ConfiguredFeature<*>): Boolean {
    val decoratedConfig = feature.config
    if (decoratedConfig !is DecoratedFeatureConfig) {
        return true
    }

    val config = decoratedConfig.feature.config
    if (config !is OreFeatureConfig) {
        return true
    }

    if (config.state.block == Blocks.IRON_ORE) {
        MoltenCoreMining.logger.debug("Removing Iron Ore")
        return false
    }

    return true
}
