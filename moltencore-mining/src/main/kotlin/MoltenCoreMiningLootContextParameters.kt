package net.braindemons.moltencore.mining

import net.minecraft.util.Identifier
import net.minecraft.world.loot.context.LootContextParameter

object MoltenCoreMiningLootContextParameters {
    val explosion = LootContextParameter<Float>(Identifier(MoltenCoreMining.modId, "explosion"))
}
