package net.braindemons.moltencore.mining

import net.minecraft.world.loot.context.LootContextParameter
import net.minecraft.world.loot.context.LootContextType

interface LootContextTypeExt {
    companion object {
        fun addAllowed(type: LootContextType, parameter: LootContextParameter<*>) {
            (type as LootContextTypeExt).moltencoreAddAllowed(parameter)
        }
    }

    fun moltencoreAddAllowed(parameter: LootContextParameter<*>)
}