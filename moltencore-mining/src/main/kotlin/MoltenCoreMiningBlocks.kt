package net.braindemons.moltencore.mining

import net.fabricmc.fabric.api.block.FabricBlockSettings
import net.minecraft.block.Material

object MoltenCoreMiningBlocks {
    val denseStone = DenseStoneBlock(
        FabricBlockSettings
            .of(Material.STONE)
            .strength(1.5F, 12.0F)
            .build()
    )
}