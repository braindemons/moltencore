package net.braindemons.moltencore.mining

import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.state.StateFactory
import net.minecraft.state.property.IntProperty
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IWorld
import net.minecraft.block.BlockRenderLayer
import net.minecraft.block.Blocks
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.world.BlockView
import net.minecraft.world.World

class DenseStoneBlock(settings: Settings) : Block(settings) {
    companion object {
        const val MAX_CONTENTS = 50
        const val MAX_PROGRESS = MAX_CONTENTS - 1
    }

    object States {
        val progress: IntProperty = IntProperty.of("progress", 0, MAX_PROGRESS)
    }

    init {
        defaultState = getStateFactory().defaultState.with(States.progress, 0)
    }

    override fun appendProperties(stateFactory: StateFactory.Builder<Block, BlockState>?) {
        stateFactory?.add(States.progress)
    }

    override fun onBroken(world: IWorld?, position: BlockPos?, state: BlockState?) {
        val prevProgress = state?.get(States.progress) ?: 0
        val nextProgress = prevProgress + 1

        if (nextProgress <= MAX_PROGRESS) {
            val nextState = MoltenCoreMiningBlocks.denseStone.defaultState.with(States.progress, nextProgress)
            world?.setBlockState(position, nextState, 0)
        }
    }
}
