package net.braindemons.moltencore.mining

object SetBlockStateFlags {
    const val NONE: Int = 0;
    const val UPDATE_NEIGHBORS: Int = 1;
    const val UPDATE_LISTENERS: Int = 2;

    // More exist but are not included
}
