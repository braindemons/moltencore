package net.braindemons.moltencore.mining

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import net.minecraft.block.Blocks
import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier
import net.minecraft.world.loot.context.LootContext
import net.minecraft.world.loot.context.LootContextParameters
import net.minecraft.world.loot.function.LootFunction

class DenseStoneLootFunction : LootFunction {
    override fun apply(p0: ItemStack, p1: LootContext): ItemStack {
        // If we didn't get destroyed by an explosion, just return the item stack
        if (!p1.hasParameter(MoltenCoreMiningLootContextParameters.explosion)) {
            return p0
        }

        // We did get destroyed by an explosion.
        // Breaking will be canceled, so we need to just drop an amount and see if we need to break manually.
        val state = p1.get(LootContextParameters.BLOCK_STATE)
        val position = p1.get(LootContextParameters.POSITION)

        val amount = 10 + p1.random.nextInt(16)
        val progress = state?.get(DenseStoneBlock.States.progress) ?: 1
        val nextProgress = progress + amount

        val flags = SetBlockStateFlags.UPDATE_NEIGHBORS.or(SetBlockStateFlags.UPDATE_LISTENERS);

        if (nextProgress <= DenseStoneBlock.MAX_PROGRESS) {
            // We don't need to break, but update the progress
            val nextState = MoltenCoreMiningBlocks.denseStone.defaultState.with(DenseStoneBlock.States.progress, nextProgress)
            p1.world.setBlockState(position, nextState, flags)

            p0.count = 10
        } else {
            // We do need to break
            p1.world.setBlockState(position, Blocks.AIR.defaultState, flags)

            // Calculate how much was remaining and drop that
            p0.count = DenseStoneBlock.MAX_CONTENTS - progress
        }

        return p0
    }

    class Factory : LootFunction.Factory<DenseStoneLootFunction>(
        Identifier(MoltenCoreMining.modId,"dense_stone_loot"),
        DenseStoneLootFunction::class.java
    ) {
        override fun toJson(var1: JsonObject?, var2: DenseStoneLootFunction?, var3: JsonSerializationContext?) {
        }

        override fun fromJson(var1: JsonObject?, var2: JsonDeserializationContext?): DenseStoneLootFunction {
            return DenseStoneLootFunction()
        }
    }
}