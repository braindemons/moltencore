package net.braindemons.moltencore.mining

import io.github.cottonmc.cotton.logging.ModLogger

object MoltenCoreMining {
    const val modId = "moltencore_mining"
    val logger = ModLogger(modId)
}
