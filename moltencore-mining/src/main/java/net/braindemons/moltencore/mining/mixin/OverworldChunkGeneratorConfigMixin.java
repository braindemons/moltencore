package net.braindemons.moltencore.mining.mixin;

import net.braindemons.moltencore.mining.MoltenCoreMiningBlocks;
import net.minecraft.world.gen.chunk.ChunkGeneratorConfig;
import net.minecraft.world.gen.chunk.OverworldChunkGeneratorConfig;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(OverworldChunkGeneratorConfig.class)
public abstract class OverworldChunkGeneratorConfigMixin extends ChunkGeneratorConfig {
    @Inject(method = "<init>", at = @At("RETURN"))
    private void onConstructed(CallbackInfo info) {
        defaultBlock = MoltenCoreMiningBlocks.INSTANCE.getDenseStone().getDefaultState();
    }
}
