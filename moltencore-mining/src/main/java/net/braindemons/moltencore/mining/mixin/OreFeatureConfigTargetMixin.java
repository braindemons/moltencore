package net.braindemons.moltencore.mining.mixin;

import net.braindemons.moltencore.mining.MoltenCoreMining;
import net.braindemons.moltencore.mining.MoltenCoreMiningBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.function.Predicate;

@Mixin(OreFeatureConfig.Target.class)
public class OreFeatureConfigTargetMixin {
    @Shadow
    @Final
    private String name;

    @Shadow
    @Final
    private Predicate<BlockState> predicate;

    @Inject(method = "getCondition", at = @At("HEAD"), cancellable = true)
    public void onGetCondition(CallbackInfoReturnable<Predicate<BlockState>> info) {
        // Our custom dense stone block needs to also be considered natural stone for the purpose of ore generation
        if (name.equals("natural_stone")) {
            Predicate<BlockState> wrapped = (blockState) -> {
                if (blockState == null) {
                    return false;
                }

                if (blockState.getBlock() == MoltenCoreMiningBlocks.INSTANCE.getDenseStone()) {
                    return true;
                }

                return this.predicate.test(blockState);
            };

            info.setReturnValue(wrapped);
        }
    }
}
