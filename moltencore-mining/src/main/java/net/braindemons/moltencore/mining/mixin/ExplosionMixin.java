package net.braindemons.moltencore.mining.mixin;

import net.braindemons.moltencore.mining.MoltenCoreMiningBlocks;
import net.braindemons.moltencore.mining.MoltenCoreMiningLootContextParameters;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion;
import net.minecraft.world.loot.context.LootContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(Explosion.class)
public abstract class ExplosionMixin {
    @Redirect(
            method="affectWorld",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/block/Block;dropStacks(Lnet/minecraft/block/BlockState;Lnet/minecraft/world/loot/context/LootContext$Builder;)V")
    )
    private void onAffectWorldBlockDropStacks(BlockState blockState, LootContext.Builder builder) {
        // Inject the custom explosion parameter
        builder.put(MoltenCoreMiningLootContextParameters.INSTANCE.getExplosion(), 4.0f);

        // Call the method we're redirecting
        Block.dropStacks(blockState, builder);
    }

    @Redirect(
            method = "affectWorld",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;setBlockState(Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;I)Z")
    )
    private boolean onAffectWorldWorldSetBlockState(World world, BlockPos blockPos_1, BlockState blockState_1, int int_1) {
        // This block handles its own breaking
        if (world.getBlockState(blockPos_1).getBlock() == MoltenCoreMiningBlocks.INSTANCE.getDenseStone()) {
            return false;
        }

        // Call the method we're redirecting
        return world.setBlockState(blockPos_1, blockState_1, int_1);
    }
}
