package net.braindemons.moltencore.mining.mixin;

import net.braindemons.moltencore.mining.MoltenCoreMiningBlocks;
import net.minecraft.block.Block;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Block.class)
public class BlockMixin {
    @Inject(method = "isNaturalStone", at = @At("HEAD"), cancellable = true)
    private static void onIsNaturalStone(Block block_1, CallbackInfoReturnable<Boolean> info) {
        if (block_1 == MoltenCoreMiningBlocks.INSTANCE.getDenseStone()) {
            info.setReturnValue(true);
        }
    }
}
