package net.braindemons.moltencore.mining.mixin;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import net.braindemons.moltencore.mining.LootContextTypeExt;
import net.minecraft.world.loot.LootTableReporter;
import net.minecraft.world.loot.context.LootContextParameter;
import net.minecraft.world.loot.context.LootContextType;
import net.minecraft.world.loot.context.ParameterConsumer;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Iterator;
import java.util.Set;

@Mixin(LootContextType.class)
public abstract class LootContextTypeMixin implements LootContextTypeExt {
    @Unique
    private Set<LootContextParameter<?>> mutableAllowed;

    @Shadow
    private Set<LootContextParameter<?>> required;

    @Inject(method = "<init>", at = @At("RETURN"))
    private void onConstructor(
            Set<LootContextParameter<?>> set_1,
            Set<LootContextParameter<?>> set_2,
            CallbackInfo info
    ) {
        mutableAllowed = Sets.newIdentityHashSet();
        mutableAllowed.addAll(Sets.union(set_1, set_2));
    }

    @Inject(method = "getAllowed", at = @At("HEAD"), cancellable = true)
    private void onGetAllowed(CallbackInfoReturnable<Set<LootContextParameter<?>>> info) {
        info.setReturnValue(mutableAllowed);
    }

    @Inject(method = "toString", at = @At("HEAD"), cancellable = true)
    private void onToString(CallbackInfoReturnable<String> info) {
        // This is just a copy with allowed changed to mutableAllowed

        Iterator stream = this.mutableAllowed.stream()
                .map((parameter) -> (this.required.contains(parameter) ? "!" : "") + parameter.getIdentifier())
                .iterator();
        String value = "[" + Joiner.on(", ").join(stream) + "]";

        info.setReturnValue(value);
    }

    @Inject(method = "check", at = @At("HEAD"), cancellable = true)
    private void onCheck(
            LootTableReporter lootTableReporter_1,
            ParameterConsumer parameterConsumer_1,
            CallbackInfo info
    ) {
        // This is just a copy with allowed changed to mutableAllowed

        Set<LootContextParameter<?>> set_1 = parameterConsumer_1.getRequiredParameters();
        Set<LootContextParameter<?>> set_2 = Sets.difference(set_1, this.mutableAllowed);
        if (!set_2.isEmpty()) {
            lootTableReporter_1.report("Parameters " + set_2 + " are not provided in this context");
        }

        info.cancel();
    }

    @Override
    public void moltencoreAddAllowed(@NotNull LootContextParameter<?> parameter) {
        mutableAllowed.add(parameter);
    }
}
