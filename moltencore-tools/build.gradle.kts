plugins {
    id("fabric-loom")
    id("org.jetbrains.kotlin.jvm")
}

fabricModProject()
applyFabricModVersion("0.1.0", sourceSets)
