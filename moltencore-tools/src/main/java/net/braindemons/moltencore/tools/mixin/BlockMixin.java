package net.braindemons.moltencore.tools.mixin;

import net.braindemons.moltencore.tools.TagUtils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Block.class)
public abstract class BlockMixin {
    @Inject(method = "calcBlockBreakingDelta", at = @At("HEAD"), cancellable = true)
    private void onCalcBlockBreakingDelta(
            BlockState blockState_1,
            PlayerEntity playerEntity_1,
            BlockView blockView_1,
            BlockPos blockPos_1,
            CallbackInfoReturnable<Float> info
    ) {
        World world = (World) blockView_1;

        // Only allow gathering logs if you have an axe
        if (TagUtils.INSTANCE.isLogs(world, blockState_1) && !TagUtils.INSTANCE.isHoldingAxe(world, playerEntity_1)) {
            info.setReturnValue(0.0f);
            return;
        }

        // Only allow gathering stone if you have a pickaxe
        if (TagUtils.INSTANCE.isUnpunchableStone(world, blockState_1) && !TagUtils.INSTANCE.isHoldingPickaxe(world, playerEntity_1)) {
            info.setReturnValue(0.0f);
            return;
        }
    }
}
