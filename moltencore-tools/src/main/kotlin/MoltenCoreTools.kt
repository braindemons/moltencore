package net.braindemons.moltencore.tools

import io.github.cottonmc.cotton.logging.ModLogger
import net.minecraft.block.Block
import net.minecraft.tag.Tag
import net.fabricmc.fabric.api.tag.TagRegistry
import net.minecraft.util.Identifier

object MoltenCoreTools {
    const val modId = "moltencore_tools"
    val logger = ModLogger(modId)
}
