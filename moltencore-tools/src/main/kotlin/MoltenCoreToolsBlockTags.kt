package net.braindemons.moltencore.tools

import net.fabricmc.fabric.api.tag.TagRegistry
import net.minecraft.block.Block
import net.minecraft.tag.Tag
import net.minecraft.util.Identifier

object MoltenCoreToolsBlockTags {
    val unpunchableStoneId: Identifier = id("unpunchable_stone")
    val unpunchableStone: Tag<Block> = register("unpunchable_stone")

    private fun id(local_id: String): Identifier {
        return Identifier(MoltenCoreTools.modId, local_id)
    }

    private fun register(local_id: String): Tag<Block> {
        return TagRegistry.block(id(local_id))
    }
}
