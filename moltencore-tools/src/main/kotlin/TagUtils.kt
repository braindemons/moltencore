package net.braindemons.moltencore.tools

import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.util.Identifier
import net.minecraft.world.World

object TagUtils {
    fun isLogs(world: World, blockState: BlockState): Boolean {
        return checkBlockContains(world, Identifier("minecraft", "logs"), blockState.block);
    }

    fun isUnpunchableStone(world: World, blockState: BlockState): Boolean {
        return checkBlockContains(
            world,
            MoltenCoreToolsBlockTags.unpunchableStoneId,
            blockState.block
        )
    }

    fun isHoldingAxe(world: World, player: PlayerEntity): Boolean {
        return isHoldingTagged(world, player, Identifier("fabric", "axes"));
    }

    fun isHoldingPickaxe(world: World, player: PlayerEntity): Boolean {
        return isHoldingTagged(world, player, Identifier("fabric", "pickaxes"));
    }

    private fun isHoldingTagged(world: World, player: PlayerEntity, tagId: Identifier): Boolean {
        val currentStack = player.inventory.main.get(player.inventory.selectedSlot);
        return checkItemContains(world, tagId, currentStack.item);
    }

    private fun checkBlockContains(world: World, tagId: Identifier, block: Block): Boolean {
        // Workaround for bug FabricMC/fabric#423
        val tag = world.tagManager.blocks().get(tagId);

        return tag?.contains(block) ?: false
    }

    private fun checkItemContains(world: World, tagId: Identifier, item: Item): Boolean {
        // Workaround for bug FabricMC/fabric#423
        val tag = world.tagManager.items().get(tagId);

        return tag?.contains(item) ?: false
    }
}